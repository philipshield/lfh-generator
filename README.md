uitar tablature is a notation system widely used by guitar players when learning to play songs. The notation consists of information about which string and which fret the guitarist should put his finger on. However, it does not normally contain information on which fingers should be put on which notes. 

In this thesis we study existing methods related to incorporating fingering information alongside musical notations. Using this material, we define a set of factors that affect the guitar fingering and then design an algorithm for computing an optimal fingering for a tablature. 

The final product is an algorithm that computes optimal fingering positions according to the relevant factors. The computed fingerings are evaluated by comparing them to how experienced guitarists play the given melody. 

Our results indicate that it is possible to produce optimal fingerings algorithmically. This is a step forward in helping beginner and intermediate guitar players in their learning process.