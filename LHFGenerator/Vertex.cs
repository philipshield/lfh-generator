﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator
{
    public class Vertex
    {
        public Note Note;
        public Finger Finger;
        public List<Edge> EdgesOut = new List<Edge>(Common.MaxEgdesOut);
        
        public Vertex(Note note, Finger finger)
        {
            Note = note;
            Finger = finger;
        }

        public void AddEdge(Vertex to)
        {
            EdgesOut.Add(new Edge(this, to, 0));
        }


        public override string ToString()
        {
            string note = Note != null ? Note.ToString() : "null";
            return note + " (" + Finger + ")";
        }


    }
}
