using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Linq.Expressions;

namespace LHFGenerator
{
    public static class Extensions
    {
        private static Random r = new Random((int)DateTime.Now.Millisecond);

        public static T GetRandom<T>(this IEnumerable<T> source)
        {
            return source.ElementAt(r.Next(0, source.Count()));
        }


        /// <summary>
        /// Provides formatting functionallity with custom keywords.
        /// Creator: Bartdesmets.
        /// </summary>
        /// <param name="source">The source string.</param>
        /// <param name="args">The formatting.</param>
        /// <example>"myString {city} , {object}".Format(city => "New York", object => new Object()</example>
        /// <returns>A formatted copy of the string.</returns>
        public static string With(this string source, params Expression<Func<string, object>>[] expressions)
        {
            // {parameters of expressions}  ----> is to be replaced by corresponding ----> "return value of expression"
            Dictionary<string, object> formatpairs = expressions.ToDictionary
                                                     (e => "{" + e.Parameters[0].Name + "}"
                                                     , e => e.Compile()(null));

            var s = new StringBuilder(source);
            foreach (var kv in formatpairs)
            {
                s.Replace(

                    kv.Key,
                    kv.Value != null
                          ? kv.Value.ToString()
                          : "");
            }

            return s.ToString();
        }
    }
}
