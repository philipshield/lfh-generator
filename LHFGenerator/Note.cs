using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator
{
    public class Note
    {
        public readonly int Fret;
        public readonly int String;
        public Finger? Finger;
        public static int DERP = 1337;

        public Note(int fret, int theString)
        {
            Fret = fret;
            String = theString;
            Finger = null;
            
        }


        public override string ToString()
        {
            return Fret.ToString();
        }


    }
}
