using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.GraphModel;
using LHFGenerator.Guitar;

namespace LHFGenerator.Rules
{
    public class StringChangeRule: Rule
    {
        public StringChangeRule()
        {
            Influence = Common.StringChangeRuleInfluence;
        }

        public override void ModifyEdge(Edge edge)
        {
            //Early Bailout
            if (!edge.IsStringChange()) return;


            int score = 0;

            //Same fret
            if (edge.IsTransitionToSameFret())
            {
                //if the change is to the next string, its easy to bar with Index-Finger
                if (Math.Abs(edge.StringDistance) == 1 && edge.From.Finger == Finger.One && edge.To.Finger == Finger.One)
                {
                    score += (int)(ScoreProvider.Translate(Score.Little));
                }

                //penalize barring with finger 2, 3 or 4
                if (Math.Abs(edge.StringDistance) == 1) //close string
                {
                    if (!(edge.IsFingerTransition(Finger.Two, Finger.Two) || edge.IsFingerTransition(Finger.Three, Finger.Three) || edge.IsFingerTransition(Finger.Four, Finger.Four)))
                    {
                        score += (int)(ScoreProvider.Translate(Score.Little));
                    }
                }
                

                //Transition to lower string
                if (edge.IsTransitionToLowerString())
                {
                    if (edge.IsTransitionToLowerFinger())
                    {
                        score += (int)(ScoreProvider.Translate(Score.Good));
                    }
                    else
                    {
                        score += (int)(ScoreProvider.Translate(Score.Little));
                    }
                }
                else //Transition to higher string
                {
                    if (edge.IsTransitionToHigherFinger())
                    {
                        score += (int)(ScoreProvider.Translate(Score.Good));
                    }
                    else
                    {
                        score += (int)(ScoreProvider.Translate(Score.Little));
                    }
                }
            }

                    //String distance(absolute value) if ==1 ==> use same finger(preferably)

            //Other fret
            else 
            {

            }


            DoModify(edge, score);
        }

    }
}
