using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.GraphModel;
using LHFGenerator.Guitar;

namespace LHFGenerator.Rules
{

    /// <summary>
    /// => Short Range
    /// for short note ranges (between 0 and maxFingerRange
    /// 
    /// => Long Range
    /// If the note range is large (larger than maxFingerRange
    /// </summary>
    public sealed class DistanceRule : Rule
    {

        public DistanceRule()
        {
            Influence = Common.DistanceRuleInfluence;
        }

        public override void ModifyEdge(Edge edge)
        {
            int noteDistance = edge.NoteDistance;
            int fingerDistance = edge.FingerDistance;
            int score = 0;
            bool fingersCrossing = !(noteDistance * fingerDistance >= 0);
            
            //Short Range
            if (Math.Abs(noteDistance) < Common.MaxFingerRange)
            {
                //Early Bailout
                if (fingersCrossing)
                {
                    Debug.WriteLine("bailout..");
                    return;
                }

                //REGULAR SHORT RANGE                    
                //1) 
                //2) 
                //3) 


                int res = Math.Abs(noteDistance - fingerDistance);
               
                if (res == 0)
                {
                    score = ScoreProvider.Max;
                }
                else
                {
                    score = translateResult(res);
                }

                //Debug.WriteLine("SHORT");
            }
            else //Long Range
            {
                Debug.WriteLine("LONG");
                //all fingerings weigh the same
                score = ScoreProvider.Translate(Score.Little); //It matters whether we skip it or weigh all the same..

            }
            

            //Do score
            DoModify(edge, score);

            //debug
            /*Debug.WriteLine("{from}({finger1}) --({w})--> {to}({finger2})".With(
                                            from => edge.From.Note, 
                                            finger1 => edge.From.Finger,
                                            w => edge.Weight, 
                                            to => edge.To.Note, 
                                            finger2 => edge.To.Finger));
             */
            //Debug.Wait(1);   
        }

        private int translateResult(int res)
        {
            //res >= 1
            if(res == 1)
            {
                return (int)ScoreProvider.Translate(Score.Good);
            }
            else if (res == 2)
            {
                return (int)ScoreProvider.Translate(Score.Avg);
            }
            else if(res == 3)
            {
                return (int)ScoreProvider.Translate(Score.AvgLower);
            }
            else if(res == 4)
            {
                return (int)ScoreProvider.Translate(Score.None);
            }
            else
            {
                return (int)ScoreProvider.Translate(Score.None);
            }

        }
    }
}
