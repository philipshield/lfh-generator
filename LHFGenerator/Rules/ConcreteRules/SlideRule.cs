using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.GraphModel;

namespace LHFGenerator.Rules
{
    public sealed class SlideRule : Rule
    {
        public SlideRule()
        {
            Influence = Common.SlideRuleInfluence;
        }

        public override void ModifyEdge(Edge edge)
        {
            int score = 0;


            //Only Slide when not string-change
            if (!edge.IsStringChange())
            {
                //always better to slide to same finger
                if (edge.From.Finger == edge.To.Finger)
                {
                    //TODO: Its better to not slide to often (dont give as much to slide if last transition also was slide)
                    
                    score += (int)(ScoreProvider.Translate(Score.Little));
                }
            }


            DoModify(edge, score);
            
        }
    }
}
