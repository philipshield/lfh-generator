﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.GraphModel;

namespace LHFGenerator.Rules
{
    /// <summary>
    /// An edge visitor that modifies an edge according to a specific rule.
    /// </summary>
    public abstract class Rule
    {
        /// <summary>
        /// Modifies the edge. Each subclass represents a concrete rule.
        /// </summary>
        /// <param name="edge">The edge that will be modified (A Trasition)</param>
        public abstract void ModifyEdge(Edge edge);
    }
}
