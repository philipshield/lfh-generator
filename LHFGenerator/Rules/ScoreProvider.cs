using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator.Rules
{
    public static class ScoreProvider
    {
        private static int min = Common.MinScore;
        private static int max = Common.MaxScore;
        private static int ScoreStep = Common.ScoreStep;

        
        public static int Max { get { return max; } }
        public static int Min { get { return min; } }
        public static int Step { get { return ScoreStep; } }

        public static int Translate(int score)
        {
            int range = Max - Min;
            int sc = (int)score;
            float p = ((float)sc)/100;
            int res = (int)Math.Round(Convert.ToDouble(p*range));
            return res;
        }

        public static int Translate(Score score) 
        {
            return Translate((int)score);
        }

        public static int Request(int value) 
        {

            if (value < min || value > max)
            {
                throw new ArgumentOutOfRangeException("score of " + value);
            }

            return value;
        }

        public static int Truncate(int value) 
        {
            if (value < min)
            {
                return min;
            }
            else if (value > max) 
            {
                return max;
            }
            else
            {
                return value;
            }
        }
    }
}
