using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator.Rules
{
    /// <summary>
    /// A human way of scoring, maps to a place on the scoring scale
    /// </summary>
    public enum Score : int
    {
        None = 0, 
        VeryLittle = 1,
        Little = 5,
        AvgLower = 25,
        Avg = 50,
        Good = 75,
        Full = 100
    }
}
