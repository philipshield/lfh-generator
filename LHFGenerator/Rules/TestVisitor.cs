using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.GraphModel;
using LHFGenerator.Rules;

namespace LHFGenerator.Rules
{
    public class DummyRule : Rule
    {
        Random r = new Random(DateTime.Now.GetHashCode());

        public override void ModifyEdge(Edge edge)
        {
            edge.AddWeight(r.Next(10));
        }
    }
}
