﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator
{

    /// <summary>
    ///  Vertex from;
	///  Vertex to;
	///  int weight;
	///  boolean visited = false; från början är den inte visited.
    ///  
    /// Skapa getters.
    /// Skapa set weight
    /// Skapa ModifyWeight(int x)
    /// Skapa toString som printar vackert :p
    /// </summary>
    public class Edge
    {
        public Vertex To;
        public Vertex From;
        public int Weight;

        public Edge(Vertex from, Vertex to, int weight)
        {
            From = from;
            To = to;
            Weight = weight;
        }

        public void AddWeight(int weight)
        {
            Weight += weight;
        }

        public void Visit(EdgeVisitor visitor) 
        {
            visitor.ModifyEdge(this);
        }

        public override string ToString()
        {
            return "{v1} --> {v2}".With(v1 => From.ToString(), v2 => To.ToString());
        }
    }
}
