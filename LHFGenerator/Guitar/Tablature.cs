using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using LHFGenerator.GraphModel;

namespace LHFGenerator.Guitar
{
    /// <summary>
    /// As for now, represents a tablature of a simple melody.
    /// </summary>
    public class Tablature
    {
        private Note[,] noteMatrix;

        private int width { get { return noteMatrix.GetLength(1); } }
        private int height { get { return noteMatrix.GetLength(0); } }

        public int Length { get { return width; } }


        public Tablature(string filename)
        {
            int buffersize = 1000;
            noteMatrix = new Note[Common.NumStrings, buffersize];
            readfromfile(filename);
        }

        #region File Reading

        private void readfromfile(string filename) 
        {
            StreamReader reader = File.OpenText("../../Tablatures/{name}".With(name => filename));
            for (int str = 0; str < Common.NumStrings; str++)
            {
                string gstring = reader.ReadLine();
                readString(gstring, str);   
            }
        }

        private void readString(string gstring, int str) 
        {
            Queue<char> chars = new Queue<char>(gstring.ToCharArray());
            char[] digits = new char[]{'1','2','3','4','5','6','7','8','9','0'};

            int pos = -1;
            while (chars.Count != 0)
            {
                char ch = chars.Dequeue();
                pos++;
                char nextch = chars.Count != 0 ? chars.Peek() : '-';

                bool firstIsDigit = digits.Contains(ch);
                bool secondIsDigit = digits.Contains(nextch);
                if (!firstIsDigit)
	            {

		            continue;
	            }
                if (secondIsDigit)
                {
                    chars.Dequeue();
                    pos++;
                    AddNote((GuitarString)str, Int32.Parse(new string(new char[]{ch, nextch})), pos);
                }
                else //single char note
	            {
                    AddNote((GuitarString)str, Int32.Parse(new string(new char[] { ch })), pos);
	            }

                
                
            }
        }


        #endregion

        public void AddNote(GuitarString str, int fret, int pos) 
        {
            this[(int)str, pos] = new Note(fret, (int)str);
        }


        /// <summary>
        /// lite halv fult..
        /// </summary>
        /// <param name="fingering"></param>
        public void ApplyFingering(List<Vertex> fingering) 
        {
            foreach (Vertex vertex in fingering)
            {
                if (vertex.Note != null)
                {
                    vertex.Note.Finger = vertex.Finger;
                }
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            const string newline = "\n";

            string[] strings = new[] { "e", "B", "C", "D", "A", "E" };
            int count = 0;

            for (int row = 0; row < height; row++)
            {
                string str = strings[count];
                sb.Append(str + " ");
                for (int column = 0; column < width; column++)
                {
                    Note note = noteMatrix[row, column];
                    string res = note != null 
                        ? note.ToString()
                        : "-";
                    if (res.Length == 1)
                    {
                        res += "-";
                    }

                    sb.Append(res);
                }
                sb.Append(newline);
                count++;
            }


            int buff = Console.BufferWidth;
            string[,] ss = new string[buff, Common.NumStrings];

            return sb.ToString();
        }

        public void WriteFingering()
        {
            Console.Write("F   ");
            for (int i = 1; i < Length; i++)
            {
                Note[] col = GetColumns().ToArray()[i];
                Finger? finger = col.All(n => n == null) ? null : col.First(n => n != null).Finger;
                Console.Write(finger != null ? ((int)finger).ToString() + " " : "  ");
            }
        }

        private Note this[int x, int y] 
        {
            get { return noteMatrix[x,y]; }
            set { noteMatrix[x,y] = value; }
        }

        public IEnumerable<Note[]> GetColumns()
        {
            for (int column = 0; column < width; column++)
            {
                Note[] bar = new Note[Common.NumStrings];
                for (int row = 0; row <height; row++)
                {
                    bar[row] = noteMatrix[row, column];
                }
                yield return bar;
            }
        }

        public IEnumerable<Note> AllNotes() 
        {

            foreach (Note[] bar in GetColumns())
            {
                foreach (Note note in bar)
                {
                    if (note != null)
                    {
                        yield return note;                        
                    }
                }
            }
        }
    }

}
