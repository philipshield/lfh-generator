using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator.Guitar
{
    /// <summary>
    /// Represents a note in a tablature.
    /// It has a position on the neck (Fret and String)
    /// and a fingering that might be unassigned
    /// </summary>
    public class Note
    {
        public readonly int Fret;
        public readonly int GuitarString;
        public Finger? Finger;

        public Note(int fret, int theString)
        {
            Fret = fret;
            GuitarString = theString;
            Finger = null;
        }

        public override string ToString()
        {
            return Fret.ToString();
        }
    }
}
