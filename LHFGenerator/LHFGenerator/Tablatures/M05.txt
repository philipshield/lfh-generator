e||--------------------------------------------------------
b||--------------------------------------------------------
g||--------------------------------------------------------
d||---5-7-5-7--5--------------5-------7-------7------5-7---
a||-------------7---------5-7-----5-7------4----5-7--------
e||--------------8-----------------------------------------

// Led Zeppelin - Ocean
// http://www.youtube.com/watch?v=LgplgyFrIGs
// chosen because it is played very intuitively. It's a quite easy song, technically

// Result



e -----------------------------------------------------------------------------------------------------------------
B -----------------------------------------------------------------------------------------------------------------
G -----------------------------------------------------------------------------------------------------------------
D ------------5---7---5---7-----5-----------------------------5---------------7---------------7-------------5---7--
A --------------------------------7-------------------5---7-----------5---7-------------4---------5---7------------
E ----------------------------------8------------------------------------------------------------------------------

F	          1   3   1   3     1 3 4                 1   3   1       1   3   4         1     4   1   3     1   3
F	          1   3   1   3     1 3 4                 1   3   1       1   3   4         1     3   1   3     1   3   

RF	          1   3   1   3     1 3 4                 1   3   1       1   3   4         1     3   1   3     1   3   



Our optimal solution is the exact same as the fingering of Jun626 that performs the song. Notable in these results
are that in order to cope with melodies this long we have applied an approximation where we assume that in a good 
fingering we will never use sole transitions that are not very good, thus getting rid of many "weak" edges in the graph.
The fact that the results still are very good indicates that the song is technically not very difficult and that some
transitions are obviosly not very likely to be used.

technically easy songs: 
	a good transition will be appreciated by many rules, thus recieving many points. 
	a less good transition is often quite bad with aspect to many rules, thus recieving few points.
	This results in high contrast between different choices of fingering and makes it very easy and also
	effective to remove edges that are not beneficial.


technically hard songs:
	A good transition is often good with respect to some rules, but not as good with respect to others. It is always
	hard to tell whether a transition will be good in the end, as the contrast between edges is very low and many edges
	are just classified "alright" or "good perhaps".
	A bad transition will not be recogizable as easy, as it is mostly within a greater context where we can tell if that 
	transition is useful.
	This results in having to keep most edges, because few is so bad that we can throw them away, and also gives many fingerings 
	that weigh the same. For instance we might have 5 fingerings that are, technically, as good.