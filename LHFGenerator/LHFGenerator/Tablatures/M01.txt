e||-------------------------------------------
b||------10 12--12----17--15--12--------15-10-
g||----7-------------------------12--16-------
d||--9----------------------------------------
a||-------------------------------------------
e||-------------------------------------------

// Steve Vai - For The Love Of God
// http://www.youtube.com/watch?v=L2I22QmNhz8&list=PL1A4B44F653C88390

//Chosen because: <reason>:
The melody is articulate meaning the player is almost forced to "decorate" the notes with different techniques. 
We do not take many of these techniques into consideration. It also has some string changes and also requires the player to reach
for the higher frets.


//Result:


e -------------------------------------------------------------------------------------------
B --------------------10----12------12----------17------15------12------------------15----10-
G --------------7-----------------------------------------------------12------16-------------
D ----------9--------------------------------------------------------------------------------
A -------------------------------------------------------------------------------------------
E -------------------------------------------------------------------------------------------

F			3   1     3     4       4           4       3       1     1       4     3     1
F			3   1     3     4       4           4       4       1     1       4     3     1
F			3   1     4     4       4           4       3       1     1       4     3     1
F			3   1     4     4       4           4       4       1     1       4     3     1
F			3   1     4     1       1           4       4       1     1       4     3     1

RF			3	1	  3     3       1           3       3       1     1       2     1     1



This result was not close to how Steve Vai plays the riff. He has chosen to stretch to the 16th fret with his third finger, while
our solutions plays that with the fourth. Another noticable difference is that the two adjacent 12-fret-notes are in reality played 
exactly as one note. 



//Discussion:

The differences from our best computed results come from the fact that the melody is played in a very unique and 
possibly unintuitive way which uses an interesting combination of fingers. This method is prefered
by the player to express a specific feeling with the melody and to adjust for the coming notes. One example are slides, where our algorithm suggests slides only when its convenient but in this particular composition the slides are rather appearing 
as artistic articulation. This means that, in the melody the slides are there because the artist likes the sound of it, not because it's convenient. 

