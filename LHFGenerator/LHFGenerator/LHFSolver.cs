using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.Guitar;
using LHFGenerator.GraphModel;

namespace LHFGenerator
{
    /// <summary>
    /// Solves the Left Hand Guitar Fingering Problem. 
    /// It does the following:
    /// * constructs a graph model of the specified tablature
    /// * weighs the graph edges
    /// * finds a fingering by calculating an optimal path
    /// </summary>
    public class LHFSolver
    {
        public Tablature Tab;
        public Graph Graph;

        public LHFSolver(Tablature tab)
        {
            Tab = tab;
            Graph = new Graph(Tab); //un-weighed graph model.
        }

        public Tablature Solve()
        {   
            //Find short Path
            DateTime start = System.DateTime.Now;
            IEnumerable<Path> paths = Graph.FindOptimalPaths(3);
            DateTime end = System.DateTime.Now;
            Console.WriteLine((end - start).TotalMilliseconds);
            Console.ReadLine();

            //Apply LHF to Tab
            Console.WriteLine(Tab);

            String writerName = "out" + Tab.Filename;
            System.IO.StreamWriter writer = new System.IO.StreamWriter(writerName);
            string output = "";
            foreach (Path path in paths)
            {
                Tab.ApplyFingering(path.Visited);
                Tab.WriteFingering(writer);

                output += Tab.FileOutput + "\n";

                Console.WriteLine();
                //Console.Write("     ({sum})".With(sum => path.Sum));
                Console.WriteLine();
                Console.WriteLine();

            }


            writer.Close();

            System.IO.File.WriteAllText(writerName, output);
            
            Console.ReadLine();
            Graph.PrintEdges();

            //Return the LHF-enhanced tab
            return Tab;
        }

    }
}
