using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.Guitar;

namespace LHFGenerator.GraphModel
{
    /// <summary>
    /// Represents a note in the graph, consisting of a note and vertices 
    /// for each possible fingering of that note.
    /// </summary>
    public class GraphNote
    {
        public Note Note;
        public Vertex[] VertexList;

        public GraphNote(Note note)
        {
            //source or sink
            if (note == null)
            {
                Note = note;
                VertexList = new Vertex[1]
                {
                    new Vertex(Note, Finger.None)
                };
            }
            else
            {

                //Real Notes
                if (note.Fret == 0)
                {
                    //Open strings are represented by a single vertex with finger.None
              
                    VertexList = new Vertex[1]
                    {
                        new Vertex(note, Finger.None)
                    };

                }
                else
                {
                    //fretted notes are represented by four vertices (each fingering)
                    VertexList = new Vertex[4] 
                    {
                        new Vertex(note, Finger.One),
                        new Vertex(note, Finger.Two),
                        new Vertex(note, Finger.Three),
                        new Vertex(note, Finger.Four)
                    };
                }
            }

        }
    }
}
