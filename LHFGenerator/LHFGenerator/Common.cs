using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator
{
    public static class Common
    {
        // Graph
        public static int MaxEgdesOut = 4;
        public static int NumStrings = 6;

        // Biomechanics
        public static int MaxFingerRange = 6;

        //Scoring
        public static float DistanceRuleInfluence = 1;
        public static float FingerPropertiesRuleInfluence = 1;
        public static float SlideRuleInfluence = 1;
        public static float StringChangeRuleInfluence = 1;

        public static int MaxScore = 100;
        public static int MinScore = 0;
        public static int ScoreStep = 1;

        //Opts
        public static int MaxDummyDFS = 3000000;
        public static float Tolerance = 0f; //Skip Paths
        public static int IgnoreEdgeThreshhold = 0; //%

    }
}