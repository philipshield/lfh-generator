using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator.Guitar
{
    /// <summary>
    /// Enum representing each guitar string on a regular, 6-string, guitar.
    /// </summary>
    public enum GuitarString
    {
        e = 0,
        B = 1,
        C = 2,
        D = 3,
        A = 4,
        E = 5
    }
}










