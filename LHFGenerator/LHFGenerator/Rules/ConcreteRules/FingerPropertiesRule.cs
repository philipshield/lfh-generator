using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.GraphModel;
using LHFGenerator.Guitar;

namespace LHFGenerator.Rules
{
    public sealed class FingerPropertiesRule : Rule
    {
        public FingerPropertiesRule()
        {
            Influence = Common.FingerPropertiesRuleInfluence;
        }

        public override void ModifyEdge(Edge edge)
        {

            int score = 0;


            //FINGER STRENGTH

            //Pinkey is generally a little weaker => TODO: User Preference?
            if(edge.To.Finger != Finger.Four)
            {
                score += (int)(ScoreProvider.Translate(Score.Little));
            }

            //Ring to pinky is a weak transition when the note distance is different from 2
            if ((edge.IsFingerTransition(Finger.Two,Finger.Four) || (edge.IsFingerTransition(Finger.Four,Finger.Two)) && edge.NoteDistance != 2))
            {

            }
            else
            {
                score += (int)(ScoreProvider.Translate(Score.Little));
            }

            // Do Score
            DoModify(edge, score);

        }
    }
}
