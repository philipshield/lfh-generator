using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.GraphModel;

namespace LHFGenerator.Rules
{
    public sealed class IgnoreEdgesRule : Rule
    {
        private List<Edge> garbageList = new List<Edge>();

        public override void ModifyEdge(Edge edge)
        {
            if (edge.Weight < ScoreProvider.Translate(Common.IgnoreEdgeThreshhold))
            {
                garbageList.Add(edge);
            }
        }


        public void RemoveEdges() 
        {
            foreach (Edge edge in garbageList)
            {
                edge.From.RemoveEdge(edge);
            }
        }
    }
}
