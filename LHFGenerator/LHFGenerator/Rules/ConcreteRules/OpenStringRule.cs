﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.GraphModel;

namespace LHFGenerator.Rules
{
    /// <summary>
    /// For now only weighs all those edges full, to be able to do IgnoreEdges-Optimization
    /// </summary>
    public sealed class OpenStringRule : Rule
    {
        public override void ModifyEdge(Edge edge)
        {
            //If to or from open
            if (edge.FromOpenNote() || edge.ToOpenNote())
            {
                DoModify(edge, ScoreProvider.Max);
            }
        }
    }
}
