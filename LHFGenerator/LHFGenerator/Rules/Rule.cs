using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.GraphModel;

namespace LHFGenerator.Rules
{
    /// <summary>
    /// An edge visitor that modifies an edge according to a specific rule.
    /// </summary>
    public abstract class Rule
    {
        /// <summary>
        /// The highest transition-score the rule gave
        /// </summary>
        public int MaxScore;


        public float Influence = 1;

        /// <summary>
        /// Modifies the edge. Each subclass represents a concrete rule.
        /// </summary>
        /// <param name="edge">The edge that will be modified (A Trasition)</param>
        public abstract void ModifyEdge(Edge edge);

        /// <summary>
        /// Modifies the edge and keeps track of the maximum score the rule gave
        /// </summary>
        /// <param name="edge">The transition</param>
        /// <param name="score">the result score. Do not give part-scores</param>
        public void DoModify(Edge edge, int score) 
        {
            int actualscore = (int)(score * Influence);

            edge.AddWeight(actualscore);
            MaxScore = MaxScore >= actualscore ? MaxScore : actualscore; //Pick Largest
        }

        public void DoModify(Edge edge, Score score) { DoModify(edge, (int)score); }
    }
}
