using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LHFGenerator.Guitar;

namespace LHFGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            //Options
            Console.BufferWidth = 3000;

            //Read Tablature
            Tablature tab = new Tablature("desc-02.txt");

            //Solve LHF Problem
            LHFSolver solver = new LHFSolver(tab);

            
            Tablature LHFTab = solver.Solve();


            Console.ReadLine();
        }
    }
}
