using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator
{
    public static class Debug
    {
        private static string db = "> ";
        public static bool DEBUG = false;

        public static void WriteLine(string s = "") 
        {
#if DEBUG
            if (DEBUG)
            {
                Console.WriteLine(db + s);
            }

#endif
        }


        public static void Wait(int x) 
        {
#if DEBUG
            if (DEBUG)
            {
                for (int i = 0; i < x; i++)
                {
                    Console.ReadLine();
                }
            }
#endif
        }
    }
}
