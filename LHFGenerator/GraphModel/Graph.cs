using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.Guitar;
using LHFGenerator.Rules;


namespace LHFGenerator.GraphModel
{

    /// <summary>
    /// Represents the LHF-problem as a graph.
    /// Reads in a tablature and extracts the notes from it, building a graph.
    /// 
    /// </summary>
    public sealed class Graph
    {

        public long sumOfDummy = 0;
        public double dummyPathsTaken =0;
        public double nrOfDummyDFS = 0;
        
        public Random rnd = new Random();
        public int Threshold = 0;
        private int nonOpenNotes = 0;
        private int maxTransitionScore = 0;
        public int pathsThrownAway = 0;

        private GraphNote source;
        private GraphNote sink;
        private List<GraphNote> graphNotes = new List<GraphNote>();
        private List<Rule> Rules = new List<Rule>();

        private IgnoreEdgesRule edgesOutRule;

        private int numberOfNotes{ get { return (graphNotes.Count-2);} }
        
        public Graph(Tablature tab)
        {
            //Add Rules
            Rules.Add(new DistanceRule());
            Rules.Add(new SlideRule());
            Rules.Add(new FingerPropertiesRule());
            Rules.Add(new StringChangeRule());
            edgesOutRule = new IgnoreEdgesRule();
            Rules.Add(edgesOutRule);

            // Construct Graph, Weigh it, and do other optimizations before we traverse it
            Console.WriteLine(">>>> Constructing graph.\n");
            constructGraph(tab);

            Console.WriteLine(">>>> Weighing the graph.");
            Weigh();

            Console.WriteLine(">>>> Estimating threshold.");
            estimateThreshold();


        }

        private void constructGraph(Tablature tab) 
        {
            // --- Add Source
            source = new GraphNote(null);
            graphNotes.Add(source); //First place

            // --- Add Rest

            //OBS: Only simple melodies
            // i.e only one note at a time
            foreach (Note note in tab.AllNotes())
            {
                //Count non-open notes. This will be used for estimating the threshold
                if (note.Fret != 0)
                {
                    nonOpenNotes++;
                }
                

                graphNotes.Add(new GraphNote(note));
            }

            //add Sink
            sink = new GraphNote(null);
            graphNotes.Add(sink); //Last place

            //add Edges and weigh them
            Console.WriteLine(">>>> Connecting all neighbouring vertices.");
            connectAll();
            
            
        }

        /// <summary>
        /// finds the total weight of a random path
        /// </summary>
        /// <returns></returns>
        private int GetRandomPathSum(Vertex vertex, int sum = 0) 
        {
            if (vertex.IsIsolated())
            {
                //We found a path
                return sum;
            }
            else
            {
                //Walk randomly
                Edge edge = vertex.EdgesOut.GetRandom();
                return GetRandomPathSum(edge.To, sum + edge.Weight);
            }
        }

        private void estimateThreshold()
        {
            Console.WriteLine(">>>> Running discovory paths.");

            nrOfDummyDFS = Math.Pow(Common.MaxEgdesOut, 0.5 * graphNotes.Count);
            if (nrOfDummyDFS > 5000000) nrOfDummyDFS = 5000000; //truncate for speed

            int maxFoundPathWeight = 0;
            for (int i = 0; i < nrOfDummyDFS; i++)
            {
                int res = GetRandomPathSum(source.VertexList[0]);
                maxFoundPathWeight = maxFoundPathWeight > res ? maxFoundPathWeight : res;
            }

            Console.WriteLine(">>>> MaxPath sum in Discovery : " + maxFoundPathWeight + " |----> nr of Paths taken " + nrOfDummyDFS);

            //Calculate maximum path weight
            foreach (Rule r in Rules)
            {
                maxTransitionScore += r.MaxScore;
            }

            int nrOfNonOpenTransitions = nonOpenNotes - 1;
            int maxPathSum = nrOfNonOpenTransitions * maxTransitionScore;



            Threshold = maxFoundPathWeight;
            //Threshold = (int)(maxPathSum * Common.Tolerance);
            Console.WriteLine(">>>> Threshold: " + Threshold + " MAXPATHSUM: " + maxPathSum);


        }

        /// <summary>
        /// Connects all vertices in a note to the next note with an edge that initially has a weight of 0.
        /// </summary>
        private void connectAll()
        {
            //Add edges between all vertices, except for the last one. The last one will not be connected to anything. 
            for (int i =0; i<graphNotes.Count-1;i++)
            {
                //For all vertices in a given note...
                foreach(Vertex vertex in graphNotes[i].VertexList)
                {
                    //...and to all vertices in the next note...
                    //...connect!
                    foreach (Vertex NextNoteVertex in graphNotes[i+1].VertexList)
                    {
                        vertex.AddEdge(NextNoteVertex);
                    }
                }
            }

        }

        /// <summary>
        /// Weighs the graph according to the complexity rules
        /// </summary>
        public void Weigh() 
        {
            
            foreach (Edge edge in GetInnerEdges())
            {
                //let all visitors visit each edge
                foreach (Rule visitor in Rules)
                {
                    edge.Modify(visitor);                    
                }
            }

            //Remove edges that the remove-visitor has collected
            edgesOutRule.RemoveEdges();

        }

        List<Path> paths;
        public IEnumerable<Path> FindOptimalPaths(int count) 
        {
            Console.WriteLine(">>>> Traversing graph to find the best path(s).");
            //do dfs and get paths with accumulated results
            int startWeight = 0;
            paths = new List<Path>();
            dfs(source.VertexList[0], new List<Vertex>(), startWeight,0);
            Console.WriteLine(">>>> Paths that were not good enought: " + pathsThrownAway);
           
            //Get best path and return it.
            paths.Sort();
            paths.Reverse();
            Path max = paths.First();

            IEnumerable<Path> optimalPaths = paths.Take(count);

            return optimalPaths;
        }

        /// <summary>
        /// Depth-first search
        /// This algorithm digs through the graph, accumulating the taken path and the collected value on its way.
        /// </summary>
        /// <param name="vertex"></param>
        /// <param name="visited"></param>
        /// <param name="accumulatedResult"></param>
        private void dfs(Vertex vertex, List<Vertex> visited,int accumulatedResult, int currentTransition)
        {
            int transitionsLeft = (numberOfNotes + 2 - 1 - currentTransition);

            //Early bailout?
            if (accumulatedResult + transitionsLeft * maxTransitionScore < Threshold && transitionsLeft != 0)
            {
                pathsThrownAway++;
                return;
            }

            if (vertex.IsIsolated())
            {
                 //Console.WriteLine(" sum: " + accumulatedResult);
                paths.Add(new Path(visited, accumulatedResult));
            }
            else
            {
                foreach (Edge edge in vertex.EdgesOut)
                {
                    int newWeight = accumulatedResult + (edge.Weight);

                    List<Vertex> branch = new List<Vertex>(visited);
                    branch.Add(edge.From);
                    dfs(edge.To, branch, newWeight, currentTransition + 1);
                    
                }
            }

        }

        /// <summary>
        /// Returns all the edges except those from and to the source and sink.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Edge> GetInnerEdges() 
        {
            return GetEdges(1, graphNotes.Count() - 2); // (-2 => minus source and sink)
        }

  
        /// <summary>
        /// Enables enumeration of all edges in the graph.
        /// It includes edges:
        /// * from vertex at index "start" 
        /// * to edges from vertex of index "end".
        /// 
        /// default is all edges.
        /// </summary>        
        public IEnumerable<Edge> GetEdges(int start = 0, int end = -1)
        {
            if (end == -1) end = graphNotes.Count();

            //Go through Graphnodes
            for (int i = start; i < end; ++i)
            {
                GraphNote graphNote = graphNotes[i];
                foreach (Vertex vertex in graphNote.VertexList)
                {
                    foreach (Edge edge in vertex.EdgesOut)
                    {
                        yield return edge;
                    }
                }
            }
        }

        public void PrintVertices()
        {
            foreach (GraphNote graphNote in graphNotes)
            {
                foreach (Vertex vertex in graphNote.VertexList)
                {
                    Console.WriteLine(vertex);
                }
            }
        }

        public void PrintEdges()
        {
            foreach (Edge edge in GetEdges())
            {
                Console.WriteLine(edge);
            }
        }

    }
}
