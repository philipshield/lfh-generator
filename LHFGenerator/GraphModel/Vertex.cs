using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.Guitar;

namespace LHFGenerator.GraphModel
{
    /// <summary>
    /// Represents a vertex in the graph, with a note and an assigned fingering.
    /// </summary>
    public class Vertex
    {
        public Note Note;
        public Finger Finger;
        public List<Edge> EdgesOut = new List<Edge>(Common.MaxEgdesOut);
        
        public Vertex(Note note, Finger finger)
        {
            Note = note;
            Finger = finger;
        }

        public void AddEdge(Vertex to)
        {
            EdgesOut.Add(new Edge(this, to, 0));
        }

        public void RemoveEdge(Edge e)
        {
            EdgesOut.Remove(e);
        }

        public bool IsIsolated() 
        {
            return EdgesOut.Count == 0;
        }


        public override string ToString()
        {
            string note = Note != null ? Note.ToString() : "null";
            return note + " (" + Finger + ")";
        }


    }
}
