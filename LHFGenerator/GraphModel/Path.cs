using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator.GraphModel
{
    public class Path : IComparable<Path>
    {
        public int Sum;
        public List<Vertex> Visited;
/// <summary>
/// A Path object that stores the visited vertices and the accumulated sum at the end of the traversal.
/// </summary>
/// <param name="visited"></param>
/// <param name="sum"></param>
        public Path(List<Vertex> visited, int sum)
        {
            Sum = sum;
            Visited = visited;
        }

        public int CompareTo(Path other)
        {
            return Sum.CompareTo(other.Sum);
        }
        
    }
}
