using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.Rules;
using LHFGenerator.Guitar;

namespace LHFGenerator.GraphModel
{

    /// <summary>
    ///  This class represents an edge between Vertices (Fingers on each note)
    ///  To construct an edge we need two vertices and an initial weight, as default = 0.
    ///  AddWeight: We can modify the weight of an edge
    ///  Visitor: Visit Edge/edges and perform a task.
    ///  toString: print the information of a current edge.
    /// </summary>
    public class Edge
    {

        public Vertex To;
        public Vertex From;
        public int Weight;

        public int StringDistance { get { return (To.Note.GuitarString - From.Note.GuitarString); } }
        public int NoteDistance { get { return To.Note.Fret - From.Note.Fret; } }
        public int FingerDistance { get { return To.Finger == Finger.None || From.Finger == Finger.None?
                                                                0 : 
                                                                To.Finger - From.Finger; } }

        public Edge(Vertex from, Vertex to, int weight)
        {
            From = from;
            To = to;
            Weight = weight;
        }

        public void AddWeight(int weight)
        {
            Weight += weight;
        }

        public void Modify(Rule rule) 
        {
            rule.ModifyEdge(this);
        }


        public bool IsFingerTransition(Finger first, Finger second) 
        {
            return (From.Finger == first && To.Finger == second);
        }

        public bool IsStringChange() 
        {
            return StringDistance != 0;
        }

        public bool IsTransitionToSameFret()
        {
            return NoteDistance == 0;
        }

        public bool IsTransitionToLowerString()
        {
            return StringDistance > 0;
        }

        public bool IsTransitionToHigherString()
        {
            return StringDistance < 0;
        }

        public bool IsTransitionToHigherFinger()
        {
            return FingerDistance > 0;
        }

        public bool IsTransitionToLowerFinger()
        {
            return FingerDistance < 0;
        }

        public override string ToString()
        {
            return "{v1} --({w})--> {v2}".With(v1 => From.ToString(), v2 => To.ToString(), w => Weight);
        }
    }
}
