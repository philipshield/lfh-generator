using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LHFGenerator.Guitar;
using LHFGenerator.GraphModel;

namespace LHFGenerator
{
    /// <summary>
    /// Solves the Left Hand Guitar Fingering Problem. 
    /// It does the following:
    /// * constructs a graph model of the specified tablature
    /// * weighs the graph edges
    /// * finds a fingering by calculating an optimal path
    /// </summary>
    public class LHFSolver
    {
        public Tablature Tab;
        public Graph Graph;

        public LHFSolver(Tablature tab)
        {
            Tab = tab;
            Graph = new Graph(Tab); //un-weighed graph model.
        }

        public Tablature Solve()
        {   
            //Find short Path
            IEnumerable<Path> paths = Graph.FindOptimalPaths(6);


            //Apply LHF to Tab
            Console.WriteLine(Tab);
            foreach (Path path in paths)
            {
                Tab.ApplyFingering(path.Visited);
                Tab.WriteFingering();
                Console.WriteLine();
                Console.Write("     ({sum})".With(sum => path.Sum));
                Console.WriteLine();
                Console.WriteLine();
            }

            

            Console.ReadLine();
            Graph.PrintEdges();

            //Return the LHF-enhanced tab
            return Tab;
        }

    }
}
