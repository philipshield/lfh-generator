﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator
{
    public abstract class EdgeVisitor
    {
        public abstract void ModifyEdge(Edge edge);
    }
}
