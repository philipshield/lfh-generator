﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LHFGenerator
{

    /// <summary>
    /// Represents the LHF-problem as a graph, with 4 nodes for each note.
    /// </summary>
    public sealed class Graph
    {
        private GraphNote source;
        private GraphNote sink;
        private List<GraphNote> graphNotes = new List<GraphNote>();
        private List<EdgeVisitor> EdgeVisitors = new List<EdgeVisitor>();

        public Graph(Tablature tab)
        {

            constructGraph(tab);
            EdgeVisitors.Add(new TestVisitor());

            PrintEdges();

        }

        private void constructGraph(Tablature tab) 
        {
            // --- Add Source
            source = new GraphNote(null);
            graphNotes.Add(source); //First place

            // --- Add Rest

            //OBS: Only simple melodies
            // i.e only one note at a time
            foreach (Note note in tab.AllNotes())
            {
                graphNotes.Add(new GraphNote(note));
            }

            //add Sink
            sink = new GraphNote(null);
            graphNotes.Add(sink); //Last place

            //add Edges (connect nodes)
            connectAll();

            
        }

        /// <summary>
        /// Connects all vertices in a note to the next note with an edge that initially has a weight of 0.
        /// </summary>
        private void connectAll()
        {
            
            //Add edges between all vertices, except for the last one. The last one will not be connected to anything. 
            for (int i =0; i<graphNotes.Count-1;i++)
            {
                //For all vertices in a given note...
                foreach(Vertex vertex in graphNotes[i].VertexList)
                {
                    //...and to all vertices in the next note...
                    //...connect!
                    foreach (Vertex NextNoteVertex in graphNotes[i+1].VertexList)
                    {
                        vertex.AddEdge(NextNoteVertex);
                    }
                    
                }
            }

        }

        public void Weigh() 
        {
            
            foreach (Edge edge in GetEdges())
            {
                //let all visitors visit each edge
                foreach (EdgeVisitor visitor in EdgeVisitors)
                {
                    edge.Visit(visitor);                    
                }
            }
        }


        public IEnumerable<Edge> GetEdges() 
        {
            foreach (GraphNote graphNote in graphNotes)
            {
                foreach (Vertex vertex in graphNote.VertexList)
                {
                    foreach (Edge edge in vertex.EdgesOut)
                    {
                        yield return edge;
                    }
                }
            }
        }


        public void PrintVertices()
        {
            foreach (GraphNote graphNote in graphNotes)
            {
                foreach (Vertex vertex in graphNote.VertexList)
                {
                    Console.WriteLine(vertex);
                }
            }
        }

        public void PrintEdges()
        {
            foreach (Edge edge in GetEdges())
            {
                Console.WriteLine(edge);
            }
        }


    }
}
